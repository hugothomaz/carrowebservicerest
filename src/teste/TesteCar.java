package teste;
import dao.CarDAO;
import pojo.Car;

public class TesteCar {

	public static void main(String[] args) {
		CarDAO dao = new CarDAO();
		
		Car carro = new Car();
		carro.setMarca("VW");
		carro.setModelo("Gol");
		carro.setFavorite(true);
		
		dao.save(carro);
	}

}
