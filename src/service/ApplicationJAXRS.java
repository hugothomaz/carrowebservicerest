package service;
import java.util.HashSet;
import java.util.Set;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

import org.glassfish.jersey.jettison.JettisonFeature;

import service.CarService;

@ApplicationPath(value = "resources")
public class ApplicationJAXRS extends Application{

	
	@Override
	public Set<Class<?>> getClasses() {
		Set<Class<?>> classes = new HashSet<>();
		classes.add(CarService.class);
		
		return classes;
	}
	
	
	
	@Override
	public Set<Object> getSingletons() {
		Set<Object> singletons = new HashSet<>();		
		singletons.add(new JettisonFeature());
		return singletons;
	}
	
}
