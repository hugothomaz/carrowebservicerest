package service;

import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.Path;

import dao.CarDAO;
import pojo.Car;

@Path("/car")
public class CarService {

	private CarDAO daoCar;
	
	@GET
	public List<Car> listCar(){
		daoCar = new CarDAO();
		
		return daoCar.findAll();
	}
	
	
}
