package dao;

import java.io.Serializable;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.Query;

import util.JPAUtil;


public abstract class GenericDAO<T extends Serializable>{

	private Class<T> aClass;
	
	
	public GenericDAO(Class<T> aClass){
		this.aClass = aClass;
	}
	
	
	protected EntityManager getEntityManager(){
		return JPAUtil.getInstance().getEntityManager();
	}
	
	
	public int count(){
	    EntityManager manager = getEntityManager();
	    manager.getTransaction().begin();
	    
	    Query query = manager.createNamedQuery("select count(c) from " + aClass.getName() + " c");
	    
	    int count = (Integer) query.getSingleResult();
	    
	    return count;
	  }
	  
	  
	  
	  public List<T> find(String jpql, Object... param){
	    EntityManager manager = getEntityManager();
	    manager.getTransaction().begin();
	    
	    Query query = manager.createQuery(jpql);
	    
	    for(int i = 0; i < param.length; i++){
	      query.setParameter(i+1, param[i]);
	    }
	    
	    @SuppressWarnings("unchecked")
	    List<T> resultList = query.getResultList();
	    
	    manager.getTransaction().commit();
	    manager.close();
	    
	    return resultList;
	  }
	  
	  
	  public T findById(int id){
	    EntityManager manager = getEntityManager();
	    manager.getTransaction().begin();
	    T entity = manager.find(aClass, id);
	    
	    manager.getTransaction().commit();
	    manager.close();
	    
	    
	    return entity;
	  }
	  
	  
	  public T findOne(String jpql, Object... param){
	    EntityManager manager = getEntityManager();
	    manager.getTransaction().begin();
	    
	    Query query = manager.createQuery(jpql);
	    
	    for(int i = 0; i<param.length; i++){
	      query.setParameter(i+1, param[i]);
	    }
	    
	    @SuppressWarnings("unchecked")
	    T entity = (T) query.getSingleResult();
	    
	    
	    manager.getTransaction().commit();
	    manager.close();
	    
	    
	    return entity;
	  }
	  
	  
	  public List<T> findAll(){
	    EntityManager manager = getEntityManager();
	    manager.getTransaction().begin();
	    
	    Query query = manager.createQuery("from " + aClass.getName());
	    @SuppressWarnings("unchecked")
	    List<T> listResulta = query.getResultList();    
	    
	    manager.getTransaction().commit();
	    manager.close();
	    
	    return listResulta;
	  }
	  
	  
	  
	  public void save(T entity){   
	    EntityManager manager = getEntityManager();
	    manager.getTransaction().begin();
	    manager.persist(entity);    
	    manager.getTransaction().commit();
	    manager.close();     
	  }
	  
	  
	  public void update(T entity){
	    EntityManager manager = getEntityManager();
	    manager.getTransaction().begin();
	    manager.merge(entity);
	    manager.getTransaction().commit();
	    manager.close();
	  }
	  
	  public void delete(int id){
	    EntityManager manager = getEntityManager();
	    
	    manager.getTransaction().begin();
	    
	    T entity = findById(id);
	    manager.remove(entity);
	    
	    manager.getTransaction().commit();	    
	    manager.close();
	  }
	
	
	
	
}
