package dao;

import java.util.List;
import javax.persistence.EntityManager;
import pojo.Car;

public class CarDAO extends GenericDAO<Car>{

	public CarDAO() {
		super(Car.class);
	}
	
	
	public List<Car> findCarForModelo(String modelo){
		EntityManager manager = getEntityManager();
		manager.getTransaction().begin();
		
		List<Car> cliente = find("from car c where c.titulo like ?", "%"+modelo+"%");
		
		return cliente;
	}
	
	
	

}
